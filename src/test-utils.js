// test-utils.jsx
import React from 'react';
import { render as rtlRender } from '@testing-library/react';
import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { reducers } from './app/store';

function render(
  ui,
  {
    preloadedState,
    store = configureStore({
      reducer: reducers,
      preloadedState,
      middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(thunk),
    }),
    ...renderOptions
  } = {},
) {
  function Wrapper({ children }) {
    return <Provider store={store}>{children}</Provider>;
  }
  return rtlRender(ui, { wrapper: Wrapper, ...renderOptions });
}

// re-export everything
export * from '@testing-library/react';
// override render method
export { render };
