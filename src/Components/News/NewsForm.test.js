import React from 'react';
import { describe, it } from '@jest/globals';
import { render, fireEvent, wait } from '../../test-utils';
import NewsForm from './NewsForm';
import { createNews, updateNews } from '../../features/news/newsSlice';
import mockAxios from '../../__mocks__/axios';
import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';

jest.mock('../Utils/Alert');

describe('News Form', () => {
  it('renders without crashing', () => {
    render(<NewsForm />);
  });

  it('render a button with text Editar when receive a news like props', async () => {
    const newsItem = {
      id: 373,
      name: null,
      content:
        '<p>Según el informe de UNICEF, la mitad de la población infantil mundial, casi mil millones de niños, vive en 33 países con los mayores riesgos relacionados con el clima</p>',
      image: 'http://ongapi.alkemy.org/storage/xF97uDvONv.jpeg',
    };
    const { getByText } = render(<NewsForm news={newsItem} />);

    expect(getByText('Editar'));
  });

  it('render a button with text Crear when form does not receive props', async () => {
    const { getByText } = render(<NewsForm />);

    expect(getByText('Crear'));
  });

  it('should validate that inputs are required after submit button clicked', async () => {
    const { getByRole, getByText } = render(<NewsForm />);

    const button = getByRole('button');

    await wait(async () => {
      fireEvent.click(button);
    });

    getByText('Por favor ingrese un título.');
    getByText('Por favor elija una categoría.');
    getByText('Por favor añada un contenido.');
    getByText('Por favor elija una imagen.');
  });

  it('render a error message when title is shorter than 4 characters and user press submit button', async () => {
    const { getByRole, getByText } = render(<NewsForm news={{ name: 're' }} />);

    const button = getByRole('button');

    await wait(async () => {
      fireEvent.click(button);
    });

    getByText('El título debe tener al menos 4 letras.');
  });

  it('should validate that input name is required after onblur event', async () => {
    const { getByTestId, getByText } = render(<NewsForm />);

    const inputName = getByTestId('input-name');

    await wait(async () => {
      fireEvent.blur(inputName);
    });

    getByText('Por favor ingrese un título.');
  });

  it('should call post http method when createNew action is dispatched and receive a fulfilled status', async () => {
    jest.mock('../../app/store.js');
    const mockData = {
      id: 373,
      name: 'faker-name',
      content: 'faker-text',
      image: 'http://ongapi.alkemy.org/storage/xF97uDvONv.jpeg',
      category_id: 10,
    };

    const middlewares = [thunk];
    const mockStore = configureMockStore(middlewares);
    const store = mockStore();

    const actionsResulted = store.getActions();

    jest.spyOn(mockAxios, 'post').mockResolvedValue({ success: true, data: mockData });
    await store.dispatch(createNews(mockData));

    expect(mockAxios.post).toHaveBeenCalledTimes(1);
    expect(actionsResulted[1].meta.requestStatus).toEqual('fulfilled');
  });

  it('should call put http method when updateNews action is dispatched', async () => {
    const mockData = {
      id: 373,
      name: 'faker-name',
      content: 'faker-text',
      image: 'http://ongapi.alkemy.org/storage/xF97uDvONv.jpeg',
      category_id: 10,
    };

    const middlewares = [thunk];
    const mockStore = configureMockStore(middlewares);
    const store = mockStore();

    store.dispatch(updateNews(mockData));

    expect(mockAxios.put).toHaveBeenCalledTimes(1);
  });

  it('should receive a status rejected when http call has errors', async () => {
    jest.mock('../../app/store.js');

    const mockData = {
      id: 373,
      name: 'faker-name',
      content: 'faker-text',
      image: 'http://ongapi.alkemy.org/storage/xF97uDvONv.jpeg',
      category_id: 10,
    };

    const middlewares = [thunk];
    const mockStore = configureMockStore(middlewares);
    const store = mockStore();

    const actionsResulted = store.getActions();

    jest.spyOn(mockAxios, 'post').mockResolvedValue({ success: false });
    await store.dispatch(createNews(mockData));

    expect(actionsResulted[1].meta.requestStatus).toEqual('rejected');
  });
});
