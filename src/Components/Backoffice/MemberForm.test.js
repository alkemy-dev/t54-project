import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import { configureStore } from '@reduxjs/toolkit';
import { combineReducers } from 'redux';
import { Provider } from 'react-redux';
import { render, fireEvent, wait, screen } from '@testing-library/react';
import membersReducer from '../../features/members/membersSlice';
import thunk from 'redux-thunk';
import MemberForm from './MemberForm';
import userEvent from '@testing-library/user-event';
import { createMember, updateMember } from '../../features/members/membersSlice';
import mockAxios from 'axios';
import configureMockStore from 'redux-mock-store';
import Swal from 'sweetalert2';

const initialState = {
  members: {
    membersList: [],
    membersSearch: [],
    loading: false,
    error: '',
  },
};

//REDUCERS

const reducers = combineReducers({
  members: membersReducer,
});

//RENDER METHOD WITH PROVIDER
function renderWithProvider(ui, { reduxState } = {}) {
  const store = configureStore({
    reducer: reducers,
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(thunk),
    preloadedState: reduxState || initialState,
  });
  return render(
    <Provider store={store}>
      <MemoryRouter>{ui}</MemoryRouter>
    </Provider>,
  );
}

jest.mock('../../app/store');
jest.mock('../Utils/Alert');

//TESTING HTTP PETITION POST METHOD AND SUCCESS AND ERROR MESSAGES

describe('Member Form Validations Tests', () => {
  it('post a new member to the http page and check alert message', async () => {
    const middlewares = [thunk];
    const mockStore = configureMockStore(middlewares);
    const store = mockStore();
    const requestDispatch = createMember.pending;
    const successDispatch = createMember.fulfilled;
    const actionsResulted = store.getActions();
    const expectedActions = [requestDispatch, successDispatch];

    jest.mock('sweetalert2', () => ({
      fire: jest.fn(),
    }));

    const mockData = [
      {
        id: 139,
        name: 'Marita Gomez',
        image: 'http://ongapi.alkemy.org/storage/VPD4pYCz7D.jpeg',
        description: '<p>Fundadora123</p>',
        facebookUrl: 'facebook.com/MaritaGomez',
        linkedinUrl: 'linkedin.com/in/MaritaGomez',
        created_at: '2021-08-29T14:35:38.000000Z',
        updated_at: '2021-08-30T21:26:31.000000Z',
        deleted_at: null,
        group_id: null,
      },
    ];

    jest.spyOn(mockAxios, 'post').mockResolvedValue({ status: 200, data: mockData });

    await store.dispatch(createMember(mockData));

    expect(mockAxios.post).toHaveBeenCalledTimes(1);

    expect(actionsResulted.length).toEqual(expectedActions.length);

    expect(actionsResulted[0].type).toEqual(expectedActions[0].type);

    const success = expect(actionsResulted[1].type).toEqual(expectedActions[1].type);

    const error =
      actionsResulted[0].type !== expectedActions[0].type &&
      actionsResulted[1].type !== expectedActions[1].type;

    if (success) {
      await expect(Swal.fire).toHaveBeenCalledWith({
        icon: 'success',
        title: 'Miembro creado',
        text: 'El miembro se ha creado correctamente',
      });
    }

    if (error) {
      await expect(Swal.fire).toHaveBeenCalledWith({
        icon: 'error',
        title: 'Error',
        text: 'Ocurrió un error al guardar, vuelve a intentarlo.',
      });
    }
  });
});

//TESTING HTTP PETITION PUT METHOD AND SUCCESS AND ERROR MESSAGES

describe('Member Form Validations Tests', () => {
  it('edit a member on the http edit page and check alert message', async () => {
    const middlewares = [thunk];
    const mockStore = configureMockStore(middlewares);
    const store = mockStore();
    const requestDispatch = updateMember.pending;
    const successDispatch = updateMember.fulfilled;
    const actionsResulted = store.getActions();
    const expectedActions = [requestDispatch, successDispatch];

    jest.mock('sweetalert2', () => ({
      fire: jest.fn(),
    }));

    const mockData = [
      {
        id: 139,
        name: 'Marita Gomez',
        image: 'http://ongapi.alkemy.org/storage/VPD4pYCz7D.jpeg',
        description: '<p>Fundadora123</p>',
        facebookUrl: 'facebook.com/MaritaGomez',
        linkedinUrl: 'linkedin.com/in/MaritaGomez',
        created_at: '2021-08-29T14:35:38.000000Z',
        updated_at: '2021-08-30T21:26:31.000000Z',
        deleted_at: null,
        group_id: null,
      },
    ];

    const mockUpdateData = [
      {
        id: 139,
        name: 'Marita Perez',
        image: 'http://ongapi.alkemy.org/storage/VPD4pYCz7D.jpeg',
        description: '<p>Fundadora123</p>',
        facebookUrl: 'facebook.com/MaritaGomez',
        linkedinUrl: 'linkedin.com/in/MaritaGomez',
        created_at: '2021-08-29T14:35:38.000000Z',
        updated_at: '2021-08-30T21:26:31.000000Z',
        deleted_at: null,
        group_id: null,
      },
    ];

    jest.spyOn(mockAxios, 'put').mockResolvedValue({ status: 200, data: mockData });

    await store.dispatch(updateMember(mockUpdateData));

    expect(mockAxios.put).toHaveBeenCalledTimes(1);

    expect(actionsResulted.length).toEqual(expectedActions.length);

    expect(actionsResulted[0].type).toEqual(expectedActions[0].type);

    const success = expect(actionsResulted[1].type).toEqual(expectedActions[1].type);

    const error =
      actionsResulted[0].type !== expectedActions[0].type &&
      actionsResulted[1].type !== expectedActions[1].type;

    if (success) {
      await expect(Swal.fire).toHaveBeenCalledWith({
        icon: 'success',
        title: 'Miembro creado',
        text: 'El miembro se ha creado correctamente',
      });
    }

    if (error) {
      await expect(Swal.fire).toHaveBeenCalledWith({
        icon: 'error',
        title: 'Error',
        text: 'Ocurrió un error al guardar, vuelve a intentarlo.',
      });
    }
  });
});

//TESTING COMPONENT RENDERS WITHOUT PROBLEMS

describe('<MemberForm />', () => {
  it('renders without crashing', () => {
    renderWithProvider(<MemberForm />, { store: { membersList: [] } });
  });
});

//VALIDATE CANT SUBMIT FORM WHEN ALL FIELDS ARE EMPTY

describe('Member Form Validations Tests', () => {
  it('renders a error message when trying to submit with all empty fields', async () => {
    const component = renderWithProvider(<MemberForm />);

    const button = component.getByRole('button');

    await wait(async () => {
      fireEvent.click(button);
      expect(screen.getAllByText('Debe llenar todos los campos.'));
    });
  });
});

//VALIDE CANT SUBMIT FORM WHEN NAME FIELD IS EMPTY

describe('Member Form Validations Tests', () => {
  it('renders a error message when trying to submit with empty name field', async () => {
    const component = renderWithProvider(<MemberForm />);

    const button = component.getByRole('button');

    const inputName = component.getByTestId('inputname');

    userEvent.type(inputName, '');

    await wait(async () => {
      await fireEvent.click(button);
      expect(screen.getByText('Debes ingresar un nombre.'));
    });
  });
});

//VALIDATE CANT SUBMIT FORM WHEN FACEBOOK FIELD IS EMPTY

describe('Member Form Validations Tests', () => {
  it('renders a error message when trying to submit with empty facebook field', async () => {
    const component = renderWithProvider(<MemberForm />);

    const button = component.getByRole('button');

    const inputFacebook = component.getByTestId('inputfacebook');

    userEvent.type(inputFacebook, '');

    await wait(async () => {
      await fireEvent.click(button);
      expect(screen.getByText('Debes ingresar una URL de Facebook.'));
    });
  });
});

//VALIDATE CANT SUBMIT FORM WHEN LINKEDIN FIELD IS EMPTY
describe('Member Form Validations Tests', () => {
  it('renders a error message when trying to submit with empty linkedin field', async () => {
    const component = renderWithProvider(<MemberForm />);

    const button = component.getByRole('button');

    const inputLinkedin = component.getByTestId('inputlinkedin');

    userEvent.type(inputLinkedin, '');

    await wait(async () => {
      await fireEvent.click(button);
      expect(screen.getByText('Debes ingresar una URL de LinkedIn.'));
    });
  });
});

//VALIDATE CANT SUBMIT FORM WHEN DESCRIPTION FIELD IS EMPTY

describe('Member Form Validations Tests', () => {
  it('renders a error message when trying to submit with empty description field', async () => {
    const component = renderWithProvider(<MemberForm />);

    const button = component.getByRole('button');

    const inputDescription = screen.getByLabelText('Descripción:');

    userEvent.type(inputDescription, '');

    await wait(async () => {
      await fireEvent.click(button);
      expect(screen.getByText('Debes ingresar una descripción.'));
    });
  });
});
